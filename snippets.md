# Snippets

## Replace special characters that affect to Semantic MediaWiki markup

If a property (eg., `[[Text::Lorem ipsum...]]`) has `[` or `]` inside its value, it breaks the property and it doesn't work.

```wikitext
[[Text::Lorem ipsum [dolor] sid...]]
```

The above code will result in:

```
[[Text::Lorem ipsum [dolor] sid...]]
```

This is because there is `[` and `]`, and it conflict with the opening and closure square brackets of the property.

If the usage of square brackets in the value of the property can't be avoided, it is possible to use nested `#replace`, a parser function. It is necessary to have this extension enabled:

```php
# This in your LocalSettings or where you load the extensions
wfLoadExtension( 'ParserFunctions' );
$wgPFEnableStringFunctions = true;
```

If the string you are going to manipulate is longer than 1000 bytes, you have to increment this limit with `$wgPFStringLengthLimit`, eg.:

```php
# 10 000 characters
$wgPF`tringLengthLimit = 10000;
```

Once you have everything configured, you can apply a single `#replace` or nested if you need to replace several characters in the same string.


**Single replace**
```wikitext
[[Text:{{#replace:Lorem ipsum [dolor sid...|[|&#93;}}]]
```

This will result in `Lorem ipsum [dolor sid` with the property correctly marked.


**Nested replaces**
```wikitex
[[Text::{{#replace:{{#replace:Lorem ipsum [dolor] sid...|]|&#93;}}|[|&#91;}}]]
```

This will result in `Lorem ipsum [dolor] sid` with the property correctly marked.

The trick is here is to replace `[` and `]` by their respective HTML codes, `&#91;` and `&#93;`.