# mediawiki_scripts

Scripts to ease repetitive tasks on a MediaWiki instance. Most of them use the MW maintenance scripts.

The main goal of this repository is to keep tracked this scripts and the possible improvements I made to them, and then clone/update it easily on my MediaWiki instances.

Some of them might be more customizable than anothers, but generally in this repository it isn't going to be my priority, because are conceived as easy-to-write scripts to ease heavy and repetitive tasks I must face.

Each script has an introductory comment in which I explained how it works and its main goal.

## Scripts
- `promoteUsersSequence.php`. 
- `createUsersFromCsv.php`