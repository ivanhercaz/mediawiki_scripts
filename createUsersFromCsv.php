<?php
/**
 * createUsersFromCsv.php
 * 
 * This script uses the MediaWiki script createAndPromote.php to create and 
 * give rights to the users specified on CSV file ($usersFile). If the user
 * doesn't exist, it creates the user.
 * 
 * How to use:
 * - $script: change the location if the createAndPromote.php has another path.
 * - $usersFile: path of the CSV file; the structure of this CSV file must be
 * user,group_right.
 *
 * Check https://www.mediawiki.org/wiki/Manual:CreateAndPromote.php
 *
 * Feel free to adapt, improve or whatever you want with this script! And remember,
 * it's unlicensed!
 */

# Location of the MediaWiki script
$root = "/var/www/w";
$script = "$root/maintenance/createAndPromote.php";
# CSV file
$usersFile = "$root/settings/scripts/users.csv";
# Results file (remember to remove it from the server)
$resultsFile = "$root/settings/scripts/results.csv";

# Combination of possible characters to generate a password for each user
$combination = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

# try-catch block to make sure the file could be opened succesfully
try {
	$csv = new SplFileObject($usersFile, 'r');
	$csvResults = new SplFileObject($resultsFile, 'w');
} catch (RuntimeException $e) {
	printf("An error happens when the script tries to open the file: %s\n", $e->getMessage());
}

while(!$csv->eof() && ($row = $csv->fgetcsv()) && $row[0] !== null) {
	# Value from the first column on the row
	$user = $row[0];
	# Value from the second column on the row
	$right = $row[1];

	# Random password generation
	$password = substr(str_shuffle($combination), 0, 8);

	# Store the password for each user
	$csvResults->fwrite("$user,$password,$right\n");

	/**
	 * --force option is in the script in case some of the users have been already created
	 * so the script only gives the already created user the specified rights
	 */
	$output = shell_exec("php $script $user $password --custom-groups $right --force");
	echo "\t$output";

	echo "Users has random generated passwords (check results.csv).\n";
	echo "Once you have the passwords shared/stored on a safe place, remove results.csv file.\n";
}
?>
