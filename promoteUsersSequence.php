<?php
/**
 * promoteUsersSequence.php
 * 
 * This script uses the MediaWiki script createAndPromote.php to give rights 
 * to a users sequence. If the user doesn't exist, it creates the user.
 * 
 * In this case a users sequence is a serie of users that share a pattern among
 * them and then they are differentiated by a number.
 *
 * Example: Editor1, Editor2, ..., Editor44, Editor45...
 *
 * How to use:
 * - $script: change the location if the createAndPromote.php has another path.
 * - $userPattern: the word shared between the users.
 * - $userFrom: you can choose if you want to start from the beginning (default
 * behaviour, 1) or in a specific one. If you want to start with a specific one,
 * replace 1 by the number on the name of the user in which you want to start the
 * script (inclusive).
 * - $usersQuantity: specify how many users are in this sequence. You can use it
 * as the end step (inclusive). You can have 60 users, but you just want to promote
 * until the user 30. 
 *
 * Check https://www.mediawiki.org/wiki/Manual:CreateAndPromote.php
 *
 * Feel free to adapt, improve or whatever you want with this script! And remember,
 * it's unlicensed!
 */

# Location of the MediaWiki script
$script = "/var/www/w/maintenance/createAndPromote.php";
# Pattern on the users accounts
$userPattern = "Editor";
# Serial number of the first editor
$userFrom = 1;
# Serial number of the last editor
$usersQuantity = 60;

for ($i = $userFrom; $i <= $usersQuantity; $i++) {
	$user = "$userPattern$i";
	echo "- $userPattern$i \n";
	$output = shell_exec("php $script $user --custom-groups smweditor --force");
	echo "\t$output";
}
?>
